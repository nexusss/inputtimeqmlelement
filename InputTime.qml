import QtQuick 2.6
import Qt.labs.controls 1.0

import QtQuick.Layouts 1.1

Item {
    height: 32
    width: 115

    property int fontSize: 10
    property real maxTime: 0
    property var colorText: "black"
    property real defaultValue: 0
    property real seconds: 0

    signal valueChanged(real value)

    onDefaultValueChanged: {
        if(defaultValue > maxTime)
            seconds = maxTime
        else
            seconds = defaultValue

//        console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!defaul value",defaultValue)

        convertToHHMMSS()
    }

    onMaxTimeChanged: checkMaxTime()

    onSecondsChanged: {
        convertToHHMMSS()
    }

    RowLayout{
        anchors.rightMargin: 0
        spacing: 0
        scale: 1
        anchors.leftMargin: 0
        anchors.fill: parent
        Rectangle{
            color: "transparent"
            Layout.minimumHeight: 15
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            border.width: 0

            TextInput {
                id: hourField

                text: "00"
                color: colorText
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                Layout.columnSpan: 0
                Layout.rowSpan: 0
                font.pointSize: fontSize

                horizontalAlignment: Text.AlignHCenter

                validator: RegExpValidator { regExp: /[0-9]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 15

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Label {
            text: ":"
            color: colorText
            Layout.rowSpan: 0
            Layout.columnSpan: 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.minimumWidth: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            font.pointSize: fontSize
        }

        Rectangle{
            color: "transparent"
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumHeight: 15
            border.width: 0

            TextInput {
                id: minField
                text: "00"
                color: colorText
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: fontSize
                horizontalAlignment: Text.AlignHCenter
                validator: RegExpValidator { regExp: /[0-5]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 15

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Label {
            text: ":"
            color: colorText
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumWidth: 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pointSize: fontSize
        }

        Rectangle{
            color: "transparent"
            Layout.minimumHeight: 15
            TextInput {
                id: secField
                text: "00"
                color: colorText
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: fontSize
                horizontalAlignment: Text.AlignHCenter
                validator: RegExpValidator { regExp: /[0-5]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumWidth: 15
            Layout.fillHeight: true

            Layout.fillWidth: true
        }

        Label {
            text: "."
            color: colorText
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Rectangle {
            color: "#00000000"
            Layout.rowSpan: 0
            Layout.fillHeight: true
            TextInput {
                id: msecField
                color: colorText
                text: "000"
                anchors.verticalCenter: parent.verticalCenter
                validator: RegExpValidator {
                    regExp: /[0-9]{0,2}\d/
                }
                font.pointSize: fontSize
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 0
                horizontalAlignment: Text.AlignHCenter
                anchors.leftMargin: 0
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 15
            Layout.minimumHeight: 15
            Layout.fillWidth: true
            Layout.columnSpan: 0
        }

        ColumnLayout {
            id: columnLayout1
            width: 100
            height: 100
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumWidth: 0
            Layout.fillHeight: true
            Layout.fillWidth: false

            Button {
                width: 23
                height: 20
                text: "+"
                Layout.maximumWidth: 25
                highlighted: true
                checkable: false
                checked: false
                Layout.fillWidth: true
                Layout.fillHeight: true
                onClicked: addSeconds()
                autoRepeat: true
            }

            Button {

                text: "-"
                Layout.maximumWidth: 25
                highlighted: true
                Layout.fillWidth: true
                Layout.fillHeight: true
                onClicked: remSeconds()
            }
        }


    }


    function addSeconds(){
//        var seconds = getSeconds()
        var sec = seconds * 1000
//        console.log("sec0",sec)
        if(sec + 1000 <= maxTime * 1000){

            sec += 1000
//            console.log("sec1",sec)
            seconds = Math.round(sec) / 1000
//            console.log("sec2",seconds)
            convertToHHMMSS()
            valueChanged(seconds)
        }
    }

    function remSeconds(){
//        var seconds = getSeconds()
        var sec = seconds * 1000
        if(sec - 1000 >= 0 ){
            sec -= 1000
            seconds = Math.round(sec ) / 1000
            convertToHHMMSS()
            valueChanged(seconds)
        }
    }
    function getSeconds(){
        var seconds = parseInt(hourField.text) * 3600
//        console.log("secondsH",seconds,hourField.text)
        seconds += parseInt(minField.text) * 60
//        console.log("secondsM",seconds,minField.text)
        seconds += parseInt(secField.text)
//        console.log("seconds",seconds,secField.text)
        seconds += parseInt(msecField.text) / 1000
        return Math.round(seconds * 1000) / 1000
    }

    function checkMaxTime(){
//        console.log("check max time",seconds,maxTime)
        if(seconds > maxTime){
            seconds = maxTime
            valueChanged(seconds)
        }
    }

    function convertToHHMMSS(){
//        console.log("convert to hhmmss",seconds,maxTime)
        checkMaxTime()

//        console.log("convert to hhmmss",seconds,maxTime)
        var hours = Math.floor(seconds / 3600);
        hourField.text = hours
        var minutes = Math.floor((seconds - (hours * 3600)) / 60);
        minField.text = minutes
        var intSec = Math.floor(seconds - (hours * 3600) - (minutes * 60))
        secField.text = intSec
//        console.log("msec seconds * 1000",seconds * 1000,intSec * 1000,seconds * 1000 - intSec * 1000 )
        msecField.text = Math.round(seconds * 1000 - intSec * 1000)
        //        console.log("hh",hours,"mm",minutes,"sec",intSec,(seconds - intSec) * 1000)
    }
}
